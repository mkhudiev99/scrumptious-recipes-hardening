from recipes.models import Recipe

from django.db import models

from django.core.validators import MaxValueValidator, MinValueValidator

from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL



# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120),
    date = models.DateTimeField(auto_now_add=True),
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, null=True,)
    recipes = models.ManyToManyField(Recipe, related_name="meal_plans")

